from fabric.api import *
from contextlib import contextmanager as _contextmanager

# the user to use for the remote commands
env.user = '###USERNAME'
# the servers where the commands are executed
env.hosts = ['###HOST']

env.directory = '###PROJECT_DIR'
env.activate = 'source ###VIRTUALEENV_ACTIVATE'

@_contextmanager
def virtualenv():
    with cd(env.directory):
        with prefix(env.activate):
            yield

def deploy():
    with virtualenv():
        run('git pull')
        run('pip install -r requirements.txt')
        run('python gmql_site/manage.py makemigrations')
        run('python gmql_site/manage.py migrate')
        run('python gmql_site/manage.py collectstatic')
