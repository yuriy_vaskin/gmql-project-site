beautifulsoup4==4.3.2
Django==1.7.7
django-appconf==1.0.1
django-compressor==1.4
django-easy-maps==0.9
django-htmlmin==0.7.0
ecdsa==0.13
Fabric==1.10.1
geopy==1.9.1
html5lib==0.999
paramiko==1.15.2
pycrypto==2.6.1
six==1.9.0
