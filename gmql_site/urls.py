from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                        url(r'^', include('gmql_website.urls', namespace='gmql_website')),
                        url(r'^admin/', include(admin.site.urls)),
)
